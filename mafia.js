"use strict";
const readlineSync = require('readline-sync');

const roles = require(`./roles.json`);
const express = require('express');
const app = express();
const server = app.listen(3000, listening);

var setup = require(`./setup/${require.main === module ? process.argv[2] : '933t'}.json`);
const menuQ = '\nWhat happened?\n' +
	            '1. Someone died\n' +
	            '2. I want to see the remaining roles\n' +
	            '3. Someone claimed a role\n' +
	            '4. Restart with the same roles\n' +
	            '5. Game ended\n';
const whichRoleQ = '\nWhich role? (e.g. Sheriff, Dragon Head, ...)\n' +
					'Enter c to Cancel\n';
const resetQ = '\nEnter y to reset\n';
var postponedRoles = {
	Town: [],
	Mafia: [],
	Triad: [],
	Neutral: []
};
var log = [];

function checkRoles() {
	var answer = readlineSync.question(whichRoleQ);
	if (answer !== 'c') {
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Town) ? findOrDeleteRoles('Town', answer) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Mafia) ? findOrDeleteRoles('Mafia', answer) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Triad) ?  findOrDeleteRoles('Triad', answer) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Neutral) ? findOrDeleteRoles('Neutral', answer) : 
		console.log(`Invalid role: ${answer}`); //refactor with sync
	}
}
function getLog() {
	return log;
}
function listCurrentRoles() {
  	for(let i = 0; i < setup.Town.length; i++) {	
		console.log(`Town ${setup.Town[i]}`); //parse it pretty
	}
	for(let i = 0; i < setup.Mafia.length; i++) {	
	    console.log(`Mafia ${setup.Mafia[i]}`); //parse it pretty
	}
	for(let i = 0; i < setup.Triad.length; i++) {	
	    console.log(`Triad ${setup.Triad[i]}`); //parse it pretty
	}
	for(let i = 0; i < setup.Neutral.length; i++) {	
	    console.log(`Neutral ${setup.Neutral[i]}`); //parse it pretty
	}
}
function listening() {
	console.log('listening. . .');
}
function findOrDeleteRoles(affil, role, del, redo) {
	if (redo) console.log(`Attempting to remove ${role} again`);
	let cats = roles[affil][role]; //[Killing]]
	let setupCats = setup[affil];
	let lrole = role.replace(/\s+/g, '').toLowerCase(); //dragonhead
	let count = 0;
	let potentialRole;
	let removed;
	for (let i = 0; i < setupCats.length; i++) {
		//Delete specific role
		let targetRole = setupCats[i];
		if (targetRole.replace(/\s+/g, '').toLowerCase() === lrole) { 
			if (del) setupCats.splice(i,1);
			removed = targetRole;
			
			break;
		}
		//Delete a type
		else if (cats.length === 1  && cats[0] === targetRole) {
			if (del) setupCats.splice(i,1);
			removed = targetRole;
			break;
		}
		//Mark roles if ambiguous
		else {
			
			for (let j = 0; j < cats.length; j++) {

				if (cats[j] === targetRole) {
					potentialRole = targetRole;
					console.log(`${targetRole}...?`);
					count++;
				}
				if (count === 2) {
					if (!redo) { 
						console.log('\nnot sure yet... postponing...\n');
						postponedRoles[affil].push(role);
					}
					return;
				}

			}
			//Delete ambiguous role if there's only one suspect
			if (count === 1) {
				if (del) setupCats.splice(setupCats.indexOf(potentialRole), 1);
				removed = potentialRole;
			}
			//Delete a random
			else if (count === 0 && targetRole === 'Random') {
				if (del) setupCats.splice(i,1);
				removed = targetRole;
			}
		}
	}
	
	
	if (removed) {
		if (del) {
			console.log(`${removed} (${role}) removed`); //Printing removed role

			//Re-execute findOrDeleteRoles for postponed roles
			if (redo) postponedRoles[affil].splice(postponedRoles[affil].indexOf(role), 1);
			for (let i = 0; !redo && i < postponedRoles[affil].length; i++) {
				const postponedRole = postponedRoles[affil][i];
				findOrDeleteRoles(affil, postponedRole, true, true);
			}
		}
		else console.log(`Could be ${removed}`);
	} 
	else {
		if (del) {
			console.log('Something is not lining up');
			var answer = readlineSync.question(resetQ);
			if (answer === 'y') {
				reset();
			}
		}
		else console.log(`${role} is not possible`);
	}
	


}
function prerun() {
	
	if (!process.argv[2]) {
		console.log('Usage: node mafia <setup_file>');
		process.exit(1);
	} else {
		listCurrentRoles();
	}
}
function reset() {
	delete require.cache[require.resolve(`./setup/${process.argv[2] || '933t'}.json`)]
	setup = require(`./setup/${process.argv[2] || '933t'}.json`);
	postponedRoles = {
		Town: [],
		Mafia: [],
		Triad: [],
		Neutral: []
	};
	console.log('Reset complete');
	listCurrentRoles();
}
function someoneDied() {
	var answer = readlineSync.question(whichRoleQ);
	if (answer !== 'c') {
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Town) ? findOrDeleteRoles('Town', answer, true) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Mafia) ? findOrDeleteRoles('Mafia', answer, true) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Triad) ?  findOrDeleteRoles('Triad', answer, true) :
		(answer.replace(/\s+/g, '').toLowerCase() in roles.Neutral) ? findOrDeleteRoles('Neutral', answer, true) : 
		console.log(`Invalid role: ${answer}`); //refactor with sync
	}
	
}

var run = () => {
	var answer = readlineSync.keyIn(menuQ, {limit: ['$<1-5>', 'pp']});
	switch (answer) {
		case '1':
			someoneDied();
			break;
		case '2':
			listCurrentRoles();
			break;
		case '3':
			checkRoles();
			break;
		case '4':
			reset();
			break;
		case '5':
			console.log('Thanks for playing');
			process.exit(0);
		case 'pp':
			console.log(postponedRoles);
			break;
		default: 
			console.log(`Invalid choice: ${answer}`);
	}
	run();
}
// if (require.main === module) {
// 	prerun();
// 	run();
// }
// else {
// 	console.log = function(d) {
// 	    log.push(d);
// 	    process.stdout.write(d + '\n');
// 	};
// }
app.use(express.static('website'));

app.get('/', enterName)
app.get('/index/:name', displayIndex);
app.get('/all', sendAll);

function enterName(req, res) {
	
}
function displayIndex(req, res) {
	var data = req.params;
	res.send(`hello ${data.name}`);
}
function sendAll(req, res) {
	res.send(roles);
}
module.exports = {
    checkRoles: checkRoles,
    listCurrentRoles: listCurrentRoles,
    findOrDeleteRoles: findOrDeleteRoles,
    prerun: prerun,
    reset: reset,
    someoneDied: someoneDied,
    run: run,
    getLog: getLog
};
