//TODO console into something
var townSel;
var mafiaSel;
var triadSel;
var neutSel;
var anySel;

var allRoles;
var roles;

var addedRoles = {
  Town: [],
  Mafia: [],
  Triad: [],
  Neutral: [],
  Any: []
};

var btnLT = {};

var roleList;
var idCount = 0;

var inputCheck;
var btnCheck;
var result;

var inputDead;
var btnDead;
var errMsg;

var postponedRoles = {
  Town: [],
  Mafia: [],
  Triad: [],
  Neutral: []
};

var answerMode = 0;

const YES = 1;
const NO = 2;
const ERR = 3;

function preload() {
  allRoles = loadJSON('allRoles.json');
  roles = loadJSON('roles.json');
}

function setup() {
  createCanvas(250, 250);
  createElement('h2', 'Enter the role claimed by the user');
  inputCheck = createInput('');
  btnCheck = createButton('check');
  result = createP('');

  createElement('h2', 'Enter the dead role');
  inputDead = createInput('');
  btnDead = createButton('dead');
  errMsg = createP('');

  btnCheck.mousePressed(checkRoles);
  btnDead.mousePressed(someoneDied);

  townSel = select('#Town');
  mafiaSel = select('#Mafia');
  triadSel = select('#Triad');
  neutSel = select('#Neutral');
  anySel = select('#Any');

  allSel = selectAll('select');

  roleList = select('#roles');

  var townRoles = allRoles.Town;
  var mafiaRoles = allRoles.Mafia;
  var triadRoles = allRoles.Triad;
  var neutRoles = allRoles.Neutral;
  var anyRoles = allRoles.Any;

  //Town
  for (var i = 0; i < townRoles.length; i++) {
    townSel.option(townRoles[i]);
  }

  //Mafia
  for (var i = 0; i < mafiaRoles.length; i++) {
    mafiaSel.option(mafiaRoles[i]);
  }

  //Triad
  for (var i = 0; i < triadRoles.length; i++) {
    triadSel.option(triadRoles[i]);
  }

  //Neutral 
  for (var i = 0; i < neutRoles.length; i++) {
    neutSel.option(neutRoles[i]);
  }

  //Any
  for (var i = 0; i < neutRoles.length; i++) {
    neutSel.option(neutRoles[i]);
  }

  //Add role
  townSel.changed(addTown);
  mafiaSel.changed(addMafia);
  triadSel.changed(addTriad);
  neutSel.changed(addNeutral);
  anySel.changed(addAny);
}

function addTown() {
  if (!townSel.value().startsWith('-')) {
    var mark = createElement('mark', 'Town').addClass('red');
    var b = createButton('X').id('btn' + idCount);
    b.mousePressed(deleteRoleForce);

    var p = createP('').id(idCount);
    p.child(mark);
    p.html(' ' + townSel.value() + ' ', true);
    p.child(b);
    var roleObj = {
      role: townSel.value(),
      p: p
    }


    var textAndBtn = [townSel.value(), b];
    addedRoles.Town.push(roleObj);
    idCount++;
  }
  townSel.value('Town');
}

function addMafia() {
  if (!mafiaSel.value().startsWith('-')) {
    var mark = createElement('mark', 'Mafia').addClass('red');
    var b = createButton('X').id('btn' + idCount);
    b.mousePressed(deleteRoleForce);

    var p = createP('').id(idCount);
    p.child(mark);
    p.html(' ' + mafiaSel.value() + ' ', true);
    p.child(b);
    var roleObj = {
      role: mafiaSel.value(),
      p: p
    }


    var textAndBtn = [mafiaSel.value(), b];
    addedRoles.Mafia.push(roleObj);
    idCount++;
  }
  mafiaSel.value('Mafia');
}

function addTriad() {
  if (!triadSel.value().startsWith('-')) {
    var mark = createElement('mark', 'Triad').addClass('red');
    var b = createButton('X').id('btn' + idCount);
    b.mousePressed(deleteRoleForce);

    var p = createP('').id(idCount);
    p.child(mark);
    p.html(' ' + triadSel.value() + ' ', true);
    p.child(b);
    var roleObj = {
      role: triadSel.value(),
      p: p
    }


    var textAndBtn = [triadSel.value(), b];
    addedRoles.Triad.push(roleObj);
    idCount++;
  }
  triadSel.value('Triad');
}

function addNeutral() {
  if (!neutSel.value().startsWith('-')) {
    var mark = createElement('mark', 'Neutral').addClass('red');
    var b = createButton('X').id('btn' + idCount);
    b.mousePressed(deleteRoleForce);

    var p = createP('').id(idCount);
    p.child(mark);
    p.html(' ' + neutSel.value() + ' ', true);
    p.child(b);
    var roleObj = {
      role: neutSel.value(),
      p: p
    }


    var textAndBtn = [neutSel.value(), b];
    addedRoles.Neutral.push(roleObj);
    idCount++;
  }
  neutSel.value('Neutral');
}

function addAny() {
  var b = createButton('X').id('btn' + idCount);
  b.mousePressed(deleteRoleForce);

  var p = createP('').id(idCount);
  p.html('Any' + anySel.value() + ' ');
  p.child(b);
  var roleObj = {
    role: anySel.value(),
    p: p
  }


  var textAndBtn = [anySel.value(), b];
  addedRoles.Any.push(roleObj);
  idCount++;
  anySel.value('Any');
}

function deleteRoleForce() {
  var id = this.id().substring(3, this.id().length);
  for (var cat in addedRoles) {
    for (var i = 0; i < addedRoles[cat].length; i++) {
      var category = addedRoles[cat];
      if (category[i].p.id() === id) {
        addedRoles[cat].splice(i, 1);
      }
    }
  }
}

function showRoles() {
  roleList.html('');
  for (var cat in addedRoles) {
    for (var i = 0; i < addedRoles[cat].length; i++) {
      roleList.child(addedRoles[cat][i].p);
    }
  }
}

function checkRoles() {
  var answer = inputCheck.value();
  if (answer !== 'c') {
    (answer.replace(/\s+/g, '').toLowerCase() in roles.Town) ? findOrDeleteRoles('Town', answer):
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Mafia) ? findOrDeleteRoles('Mafia', answer) :
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Triad) ? findOrDeleteRoles('Triad', answer) :
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Neutral) ? findOrDeleteRoles('Neutral', answer) :
      result.html(`Invalid role: ${answer}`); //refactor with sync

  }
}

function someoneDied() {
  var answer = inputDead.value();
  if (answer !== 'c') {
    (answer.replace(/\s+/g, '').toLowerCase() in roles.Town) ? findOrDeleteRoles('Town', answer, true):
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Mafia) ? findOrDeleteRoles('Mafia', answer, true) :
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Triad) ? findOrDeleteRoles('Triad', answer, true) :
      (answer.replace(/\s+/g, '').toLowerCase() in roles.Neutral) ? findOrDeleteRoles('Neutral', answer, true) :
      result.html(`Invalid role: ${answer}`); //refactor with sync
  }

}

function findOrDeleteRoles(affil, role, del, redo) {
  var lrole = role.replace(/\s+/g, '').toLowerCase(); //dragonhead
  var cats = roles[affil][lrole]; //[Killing]]
  var availRoles = [];
  for (var i = 0; i < addedRoles[affil].length; i++) {
    availRoles.push(addedRoles[affil][i].role);
  }

  var count = 0;
  var potentialRole;
  var removed;
  for (var i = 0; i < availRoles.length; i++) {
    //Delete specific role
    var targetRole = availRoles[i];
    if (targetRole.replace(/\s+/g, '').toLowerCase() === lrole) { //if exact role is matched
      if (del) {
        addedRoles[affil].splice(i, 1);
        availRoles.splice(i, 1);
      }
      removed = targetRole;

      break;
    }
    //Delete a type
    else if (cats.length === 1 && cats[0] === targetRole) { //if only one type available
      if (del) {
        addedRoles[affil].splice(i, 1)
        availRoles.splice(i, 1);
      }
      removed = targetRole;
      break;
    }
    //Mark roles if ambiguous
    else {

      for (var j = 0; j < cats.length; j++) {

        if (cats[j] === targetRole) {
          potentialRole = targetRole;
          count++;
        }
        if (count === 2) {
          if (!redo) {
            createP('Not sure yet...');
            postponedRoles[affil].push(role);
          }
          return;
        }

      }
      //Delete ambiguous role if there's only one suspect
      if (count === 1) {
        if (del) {
          addedRoles[affil].splice(availRoles.indexOf(potentialRole), 1);
          availRoles.splice(availRoles.indexOf(potentialRole), 1);
        }
        removed = potentialRole;
      }
      //Delete a random
      else if (count === 0 && targetRole === 'Random') {
        if (del) {
          addedRoles[affil].splice(i, 1);
          availRoles.splice(i, 1);
        }
        removed = targetRole;
      }
    }
  }


  if (removed) {
    if (del) {
      console.log(`${removed} (${role}) removed`); //Printing removed role

      //Re-execute findOrDeleteRoles for postponed roles
      if (redo) postponedRoles[affil].splice(postponedRoles[affil].indexOf(role), 1);
      for (var i = 0; !redo && i < postponedRoles[affil].length; i++) {
        var postponedRole = postponedRoles[affil][i];
        findOrDeleteRoles(affil, postponedRole, true, true);
      }
    } else {
      answerMode = YES;
      result.html('Could be ' + removed);
    }
  } else {
    if (del) {
      answerMode = ERR;
      errMsg.html('No roles to eliminate for ' + role);
    } else {
      answerMode = NO;
      result.html('LIAR! ' + role + ' is not possible');
    }
  }
}

function drawYes() {
  stroke(0, 255, 0);
  strokeWeight(6);
  // fill(0, 255, 0);
  ellipse(width / 2, height / 2, 100, 100);
}

function drawNo() {
  stroke(255, 0, 255);
  strokeWeight(6);
  // fill(255, 0, 255);
  line(width / 4, height / 4, width / 2, height / 2);
  line(width / 4, height / 2, width / 2, height / 4);
}

function drawQuestion() {
  stroke(255, 255, 0);
  strokeWeight(6);
  // fill(255,255,0);
  arc(width / 2, height / 2, 200, 200, -HALF_PI, HALF_PI);
}

function draw() {
  background(153);
  if (answerMode === 1) {
    drawYes();
  } else if (answerMode === 2) {
    drawNo();
  } else if (answerMode === 3) {
    drawQuestion();
  }
  showRoles();
}